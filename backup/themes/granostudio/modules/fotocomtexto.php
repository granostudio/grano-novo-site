<?php

/**
* Módulo:
* ***** Banner - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_fotocomtexto($fotocomtexto_id, $fotocomtexto_texto, $fotocomtexto_pos, $key){
  ?>
    <div id="fotocomtexto" class="container fotocomtexto-<?php echo $key;?> ">
      <div class="row">
        <?php
        if ($fotocomtexto_pos == "direita") {
          ?>

          <div class="col-sm-6 img">
            <?php
              $img_url = wp_get_attachment_image_src($fotocomtexto_id, 'large');
             ?>
             <img src="<?php echo $img_url[0] ?>" alt="" />
          </div>
          <div class="col-sm-6 texto">
            <?php echo $fotocomtexto_texto; ?>
          </div>

          <?php
        } elseif ($fotocomtexto_pos == "esquerda") {
          ?>

          <div class="col-sm-6 texto">
            <?php echo $fotocomtexto_texto; ?>
          </div>
          <div class="col-sm-6 img">
            <?php
              $img_url = wp_get_attachment_image_src($fotocomtexto_id, 'large');
             ?>
             <img src="<?php echo $img_url[0] ?>" alt="" />
          </div>

          <?php
        }
         ?>

      </div>
    </div>
<?php
}
 ?>
