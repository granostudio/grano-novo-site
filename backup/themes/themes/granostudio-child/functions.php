<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */




/**
 * Enqueues scripts and styles.
 *
 */

function granostudio_scripts_child() {

	//Desabilitar jquery
	wp_deregister_script( 'jquery' );

	// Theme stylesheet.
	wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
	// import fonts (Google Fonts)
	wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
	// Theme front-end stylesheet
	wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

	// scripts js
	wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);
	// network effet home
	  if (is_front_page()){
			wp_enqueue_script('granostudio-network', get_stylesheet_directory_uri() . '/js/network-ani.js', '000001', false, true);
	  }
}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
