<?php
/**
* Page Template
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

get_header();?>

<?php
      if (is_child_theme()) {
        include 'wp-content/themes/granostudio-child/modules/banner-home.php';
      } else {
        include 'modules/banner-home.php';
      }

			module_bannerconteudo(40, 1);

		  // modulo Posts
			module_posts();


      echo '<div class="cadastrar"><p>Cadastre-se para ter acesso a conteúdos exclusivos</p></div>';
      echo do_shortcode('[mc4wp_form id="282"]');

 ?>

<?php get_footer(); ?>
