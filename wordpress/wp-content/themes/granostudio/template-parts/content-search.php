<?php
/**
 * The template used for displaying result search
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-sm-12 post'); ?>>

<div class="post-border">
	<?php

	if ( has_post_thumbnail() ) {

		$thumb_id = get_post_thumbnail_id(get_the_ID());
		$thumb_url = wp_get_attachment_image_src($thumb_id, 'large');
		echo '<div class="single-post-thumb" style="background-image: url('.$thumb_url[0].');height:180px;"></div>';

	}
	else {
		echo '<div class="single-post-thumb thumb-branca" style="width:100%; height:180px;background-color:lightgray;"></div>';
	}
	?>

	<div class="social-share <?php echo 'thumb';?>" style="z-index: 0;">
		Compartilhe
		<?php GranoSocialShare(get_the_permalink()) ?>
	</div>

<div class="post-inner" style="padding-top:1em;padding-left:1.3em;padding-right:1.3em;">

	<?php
	$categories = get_the_category( get_the_ID() );
	$i=1;
	$cats = array();
	foreach( $categories as $category ) {
		$cat = get_category( $category );
		$cats = array( 'name' => $cat->name, 'slug' => $cat->slug, 'link'=> $cat->term_id );
		$catlink = get_category_link($cats['link']);
		if($i==count($categories)){
			if ($cats['slug']!="sem-categoria") {
				echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>";
			}else{
				echo "<a href='".$catlink."' class='categoria'></a>";
			}
		} else {
			if ($cats['slug']!="sem-categoria") {
				echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>".', ';
			}else{
				echo "<a href='".$catlink."' class='categoria'></a>";
			}
		}
		$i++;
	}
 	?>

	<h4>
		<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title();?></a>
	</h4>

	<?php
	if ( function_exists( 'sharing_display' ) ) {
		remove_filter( 'the_content', 'sharing_display', 19 );
		remove_filter( 'the_excerpt', 'sharing_display', 19 );
	}
	the_excerpt(); ?>

</div>

</div>

 <hr>

</article><!-- #post-## -->
