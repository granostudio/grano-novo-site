<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */


?>

	<!-- HEADER -->
	<!DOCTYPE html>

	<html lang="UTF-8" >
	<head>
	<base href="/">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php wp_head(); ?>


		<!-- METATAGS -->
		<meta property="og:url" content="<?php echo get_permalink(); ?>" />
		
		<?php // site title
		$blog_title = get_bloginfo( 'name' );
		if(is_home(	)){
		?>
		<meta property="og:title" content="<?php echo $blog_title; ?>" />
		<?php
		} else{
		?>
		<meta property="og:title" content="<?php echo get_the_title(); ?>" />
		<?php
		}
		?>

		<meta property="og:type" content="website" />

		<?php if(is_home(	)){
		// site description
		$blog_desc = get_bloginfo( 'description' );
		?>
		<meta property="og:description" content="<?php echo $blog_desc; ?>" />
		<?php
		} else{
		?>
		<meta property="og:description" content="<?php echo get_the_content(); ?>" />
		<?php  }  ?>

		<meta property="og:site_name" content="<?php echo $blog_title; ?>" />

		<?php
		//imagem default ou thumbnail post
		$urlimg = '';
		$id = get_the_ID();
		if ( has_post_thumbnail($id) ) {
			$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
			$urlimg = $large_image_url[0];
		 } else {
			$urlimg = grano_get_options('seo','seo_img');
		}
		?>
		<meta property="og:image" content="<?php echo $urlimg; ?>" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="200" />
		<meta property="og:image:height" content="200" />

		<!-- <base href=" /" dirName=" " lang="<?php //echo $lang ?>" /> -->

		<?php
		$favicon = grano_get_options('design','design_favicon');
		 ?>   
		<link rel="shortcut icon" href="<?php echo $favicon; ?>">

	    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css">			

		<!-- /METATAGS --> 


		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-70037167-2"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-70037167-2');
		</script>


		<!-- Hotjar Tracking Code for http://granostudio.com.br/ -->
		<script>
		    (function(h,o,t,j,a,r){
		        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
		        h._hjSettings={hjid:891710,hjsv:6};
		        a=o.getElementsByTagName('head')[0];
		        r=o.createElement('script');r.async=1;
		        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
		        a.appendChild(r);
		    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
		</script>


		<script>
				$(document).ready(function(){
				  $('[data-toggle="tooltip"]').tooltip();
				});
		</script>


		<!--	// /Type writer effect ======================================== -->

		<script type="text/javascript">

			var TxtType = function(el, toRotate, period) {
			    this.toRotate = toRotate;
			    this.el = el;
			    this.loopNum = 0;
			    this.period = parseInt(period, 10) || 2000;
			    this.txt = '';
			    this.tick();
			    this.isDeleting = false;
			};

			TxtType.prototype.tick = function() {
			    var i = this.loopNum % this.toRotate.length;
			    var fullTxt = this.toRotate[i];

			    if (this.isDeleting) {
			    this.txt = fullTxt.substring(0, this.txt.length - 1);
			    } else {
			    this.txt = fullTxt.substring(0, this.txt.length + 1);
			    }

			    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

			    var that = this;
			    var delta = 200 - Math.random() * 100;

			    if (this.isDeleting) { delta /= 2; }

			    if (!this.isDeleting && this.txt === fullTxt) {
			    delta = this.period;
			    this.isDeleting = true;
			    } else if (this.isDeleting && this.txt === '') {
			    this.isDeleting = false;
			    this.loopNum++;
			    delta = 500;
			    }

			    setTimeout(function() {
			    that.tick();
			    }, delta);
			};

			window.onload = function() {
			    var elements = document.getElementsByClassName('typewrite');
			    for (var i=0; i<elements.length; i++) {
			        var toRotate = elements[i].getAttribute('data-type');
			        var period = elements[i].getAttribute('data-period');
			        if (toRotate) {
			          new TxtType(elements[i], JSON.parse(toRotate), period);
			        }
			    }
			    // INJECT CSS
			    var css = document.createElement("style");
			    css.type = "text/css";
			    css.innerHTML = ".typewrite > span { border-right: 0.08em solid #fff}";
			    css.innerHTML += ' @keyframes typewrite { from{border-right-color: #fff;} to{border-right-color: transparent;}}';
			    css.innerHTML += ' .typewrite > span {animation: 1s typewrite infinite; }';
			    document.body.appendChild(css);
			};

			
		</script>

		<!--	// /Type writer effect ======================================== -->

</head><!-- /Header --> 


<body ng-app="wp" style="font-family:Muli, sans-serif;">

<div >
<!-- Header Menu -->
<?php include 'header-menu.php' ?>
<!-- /Header Menu -->

<!-- Scroll Animation Grano -->
<div class="scroll-pointer"></div>
<div class="scroll-ani"></div>
