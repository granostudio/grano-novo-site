<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
?>
<?php wp_footer(); ?>

<script src="jquery.min.js"></script>
<script src="owlcarousel/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>

<?php
// livereload
$host = $_SERVER['HTTP_HOST'];
if ($host == "localhost:9001") {
  ?>
<script src="http://localhost:35729/livereload.js"></script>
  <?php
}

// /livereload
 ?>


<!-- FOOTER ============================================================== -->

<footer>

    <div class="container form-contato" data-aos="fade-up">
        <div class="row">
            <div class="col">
                <h2 class="text-center margem-b-40" style="font-size:24px;color:rgb(49,52,55);"><strong>Entre em contato</strong></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="mapa-google">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3653.9793324095317!2d-46.53730028524139!3d-23.67669708462579!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce426818e51721%3A0xc54e99e693c11ddb!2sR.+Maca%C3%BAba%2C+425+-+Para%C3%ADso%2C+Santo+Andr%C3%A9+-+SP!5e0!3m2!1spt-BR!2sbr!4v1525373194463" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <h5>Informações de contato</h5>
                <p>Rua Macaubá, 425 - Paraiso - CEP:&nbsp;09190-650 - Santo André, SP<br>Telefone: 11 3380 1454 - OurHouse Coworking</p>
            </div>
            <div class="col-12 col-md-6">
                <!-- <div class="form-group"><input type="text" placeholder="Nome" style="background-color:rgb(239,239,239);"></div>
                <div class="form-group"><input type="email" placeholder="Email"></div>
                <div class="form-group"><input type="tel" placeholder="Telefone"></div>
                <div class="form-group">
                    <textarea placeholder="Escreva sua mensagem"></textarea>
                </div>
                <button class="btn btn-primary" type="button">Enviar</button> -->
                <?php echo do_shortcode('[contact-form-7 id="481" title="Contato"]'); ?>
            </div>
        </div>
    </div>
    
    <div data-aos="fade-up" class="footer-dark">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3 item">
                    <h3>Nossas especialidades</h3>
                    <ul> 
                        <li>Branding</li>
                        <li>Identidade Visual</li>
                        <li>Web Design</li>
                        <li>Marketing Digital</li>
                        <li>Materiais Gráficos</li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3 item" style="padding-left: 0;">
                    <h3>Grano Studio</h3> 
                    <ul>
                        <li><a href="">Home</a></li>
                        <li><a class="btn-portfolio">Portfólio</a></li>
                        <li><a href="/blog/">Blog</a></li>
                    </ul>
                </div>
                <div class="col-md-6 item text">
                    <h3>Nossa missão</h3>
                    <p>Criar relações sustentáveis, justas e transparentes com todos os nossos Stakeholders.</p>
                </div>
                <div class="col-sm-12 item social">
                    <a href="https://www.facebook.com/granostudio/" target="_blanck"><i class="icon ion-social-facebook"></i></a>
                    <a href="https://twitter.com/GranoStudio" target="_blanck"><i class="icon ion-social-twitter"></i></a>
                    <a href="https://www.instagram.com/granostudio/" target="_blanck"><i class="icon ion-social-instagram"></i></a>
                    <a href="https://pt.linkedin.com/company/grano-studio" target="_blanck"><i class="icon ion-social-linkedin"></i></a>
                </div>
            </div>
            <p class="copyright">Grano Studio © 2013</p>
        </div>
    </div>

  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Clique para retornar ao topo da página" data-toggle="tooltip" data-placement="left"><span class="fa fa-angle-up"></span></a>

</footer>



<!-- /FOOTER ============================================================== -->
</div><!-- /.container -->



<!-- Google Analytics -->
<?php $seoanalytics = grano_get_options('seo','seo_analytics');

if(!empty($seoanalytics) || $seoanalytics != "Default Text"){
  echo "<script>";
  echo $seoanalytics;
  echo "</script>";
}

 ?>
<!-- / Google Analytics -->
</div> <!-- / app angular -->
</body>



<!-- Preloader -->
<script type="text/javascript">
    // //<![CDATA[
    //     $(window).on('load', function() { // makes sure the whole site is loaded
    //         $('#status').fadeOut(); // will first fade out the loading animation
    //         $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    //         $('body').delay(350).css({'overflow':'visible'});
    //     })
    // //]]>
</script>

</html>
