<!-- NAV BAR ============================================================== -->

<nav class="navbar navbar-fixed-top navbar-default" role="navigation" id="navbar">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar bar1"></span>
        <span class="icon-bar bar2"></span>
        <span class="icon-bar bar3"></span>
      </button>
      <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
          <?php
          $logo_image_url = grano_get_options('design','design_logo');
          if(empty($logo_image_url) || $logo_image_url == "http://default"){
            echo bloginfo('name');

          }else{
            echo '<img src="'.$logo_image_url.'" alt="'.get_bloginfo('name').'" class="navbar-brand-img" />';
          }
           ?>
        </a>
    </div>

</nav>    

<div class="menu-hamburger">
    <?php
        wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'menu-novo',
            'container_id'      => 'menu-novo',
            'menu_class'        => 'navbar-novo',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
        );
    ?> 

    <ul class="ml-auto">
        <li class="nav-item" role="presentation">
          <a class="nav-link active" href="#"><strong>Assine nossa newsletter</strong></a>
        </li>
        <li class="nav-item" role="presentation">
          <a class="nav-link" href="https://www.facebook.com/granostudio/"><i class="fa fa-facebook-f"></i></a>
        </li>
        <li class="nav-item" role="presentation" style="width:40px;">
          <a class="nav-link" href="https://twitter.com/GranoStudio"><i class="fa fa-twitter"></i></a>
        </li>
        <li class="nav-item" role="presentation" style="width:40px;">
          <a class="nav-link" href="https://www.instagram.com/granostudio/"><i class="fa fa-instagram"></i></a>
        </li>
        <li class="nav-item" role="presentation" style="width:40px;">
          <a class="nav-link" href="https://pt.linkedin.com/company/grano-studio"><i class="fa fa-linkedin"></i></a>
        </li>
    </ul> 

</div>



<!-- /NAV BAR ============================================================== -->
