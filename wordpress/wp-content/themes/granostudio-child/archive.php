<?php
/**
 * The template for displaying archive pages
 *
 *
 */

get_header(); ?>

<?php if (is_archive()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

<!-- Page Content -->
    <div class="container">

        <div class="article-list">

            <div class="header-archive">
                <div class="container">
                    <h1 class="page-header">
                        <?php
                            the_archive_title( '<small style="color: #0D2D25;">', '</small>' );
                            the_archive_description( '<small>', '</small>' );
                         ?>
                        <!-- <small>Secondary Text</small> -->
                    </h1> 
                        
                    <div class="categorias"> 
                        <p>Sugestões de categorias: </p>
                        <?php wp_list_categories( array(
                            'orderby'    => 'name',
                            'title_li' => false
                        ) ); ?> 
                    </div>
                </div>
            </div>
            

            <div class="articles">

                <?php
                if( have_posts() ) {
                  while ( have_posts() ) {
                    the_post(); ?>
                    
                    <div class="col-sm-6 col-md-4 item">
                        <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
                        </a> 
                        <h3 class="name"><?php echo get_the_title(); ?></h3>
                        <p class="description"><?php echo the_excerpt_max_charlength(200); ?></p>
                        <a href="<?php echo get_the_permalink(); ?>" class="action" style="background-color:#68a852;">
                            <i class="fa fa-angle-right" style="color:rgb(246,248,251);font-size:17px;padding:0px;/*line-height:-26px;*/"></i>
                        </a>
                    </div>


                  <?php }
                } else {
                  /* No posts found */
                } ?>
                
            </div>
            <!-- Pager -->
            <div class="row">
                <ul class="pager">

                    <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                    <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                </ul>
            </div>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

	
<?php get_footer(); ?>
