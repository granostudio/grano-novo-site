<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


<?php if (is_search()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

<!-- Page Content -->
    <div class="container">

        <div class="row article-list">

            <!-- Blog Entries Column -->
            <div class="" style="margin-top: 140px;">

                <div>
                    <h1 class="page-header header-search">
                        Você pesquisou por: 
                        <?php printf( __( '<span class="glyphicon glyphicon-search" aria-hidden="true"></span> %s', 'twentysixteen' ), '<span style="color: #0D2D25;">' . esc_html( get_search_query() ) . '</span>' ); ?>
                    </h1>
                    
                </div>

                <div class="articles">

                    <?php
                    if( have_posts() ) {
                      while ( have_posts() ) {
                        the_post(); ?>
                        
                        <div class="col-sm-6 col-md-4 item">
                            <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
                            </a> 
                            <h3 class="name"><?php echo get_the_title(); ?></h3>
                            <p class="description"><?php echo the_excerpt_max_charlength(200); ?></p>
                            <a href="<?php echo get_the_permalink(); ?>" class="action" style="background-color:#68a852;">
                                <i class="fa fa-angle-right" style="color:rgb(246,248,251);font-size:17px;padding:0px;/*line-height:-26px;*/"></i>
                            </a>
                        </div>                                             


                      <?php } ?>

                        <!-- Pager -->
                        <ul class="pager row">

                            <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                            <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                        </ul>

                        <div style="margin: 0px 15px;">

                            <h4 style="float: left;">Não encontrou o que desejava?</h4>
                            <form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="<form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                <div class="form-group">
                                    <label for="search-field">
                                        <i class="fa fa-search" style="color:#68a852;"></i>
                                    </label>
                                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
                                    <a class="btn btn-light action-button" role="button" href="#" style="font-family:Muli, sans-serif;background-color:#68a852;">Buscar</a></div>
                                </div>
                            </form> 
                            
                        </div>

                    <?php } else { ?>

                        <div class="else-busca">
                    
                            <h4 style="float: left;">Não há resultados encontrados, tente fazer uma nova pesquisa:</h4>

                            <form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="<form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                <div class="form-group">
                                    <label for="search-field">
                                        <i class="fa fa-search" style="color:#68a852;"></i>
                                    </label>
                                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
                                    <a class="btn btn-light action-button" role="button" href="#" style="font-family:Muli, sans-serif;background-color:#68a852;">Buscar</a></div>
                                </div>
                            </form> 

                        </div>

                    <?php } ?>
                    
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
