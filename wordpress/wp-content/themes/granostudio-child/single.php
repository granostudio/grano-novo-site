<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<?php if (is_single()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>


<?php if (is_single()) { ?> 
  
<style type="text/css">
  #menu-item-462 a{
    color: #68A852;
  }
  .progress-container{
    display: block;
  }
  .navbar{
    border: none;
  }
</style>

<?php } ?>

<div class="container"> 


    <!-- Blog Post Content Column -->
    <div class="single-blog">

        <!-- Blog Post -->

        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

        <div class="row margem-t-b-40 borda-bottom"> 
            <div class="col">
                <ul class="lista-categoria">
                    <?php
                      foreach((get_the_category()) as $category) {
                        echo '<li>' . $category->cat_name . '</li>';
                      }
                     ?>
                </ul>
                <p style="width:auto;font-family:Muli, sans-serif;float:left;">| <?php echo do_shortcode('[rt_reading_time label="Tempo de leitura:" postfix="minutos" postfix_singular="minuto"]'); ?></p>
                <!-- <p style="width:auto;font-family:Muli, sans-serif;float:left;">| Tempo de leitura: X minutos</p> -->
            </div>
        </div>

        <!-- Post Content -->

        <div class="row padding-10" data-aos="fade-up">
            <div class="col-md-9">
                <h1><?php echo get_the_title(); ?></h1>
                <p class="lead margem-t-20"></p>
                
                <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" width="100%">

                <div class="margem-t-20"><?php the_content(); ?></div>
                
                <div class="row share-row">
                    <div class="col-sm-12">
                        <p>Compartilhe:&nbsp;
                            <a href="#" class="share-icons"><i class="fa fa-facebook-f"></i></a>
                            <a href="#" class="share-icons"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="share-icons"><i class="fa fa-linkedin"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>


        <div class="article-list">
            <div class="container" data-aos="fade-up">
                <div class="intro"></div>
                <div class="row margem-t-b-20 borda-bottom">
                    <div class="col">
                        <h4>Últimas postagens</h4>
                    </div>
                </div>

                <div class="row articles">

                    <?php
                     $args = array( 'post_type' => 'post', 'posts_per_page' => 3);
                     $loop = new WP_Query( $args );

                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


                    <div class="col-sm-6 col-md-4 item">
                        <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
                        </a> 
                        <h3 class="name"><?php echo get_the_title(); ?></h3>
                        <p class="description"><?php echo the_excerpt_max_charlength(200); ?></p>
                        <a href="<?php echo get_the_permalink(); ?>" class="action" style="background-color:#68a852;">
                            <i class="fa fa-angle-right" style="color:rgb(246,248,251);font-size:17px;padding:0px;/*line-height:-26px;*/"></i>
                        </a>
                    </div>

                    <?php endwhile; // end of the loop. ?>
                    <?php endif; ?>

                </div>
            </div>

        </div>

    </div>

    <?php endwhile; // end of the loop. ?>
        

</div>
<!-- /.container -->

<?php get_footer(); ?>
