<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
 

add_theme_support( 'post-thumbnails' );


// offset the main query on the home page
function tutsplus_offset_main_query ( $query ) {
     if ( $query->is_home() && $query->is_main_query() ) {
         $query->set( 'offset', '1' );
    }
 }


 //  O número 80 é a quantidade de caracteres a exibir.
function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
	}



// =========================== Custom post type	=========================== // 

 function custom_post_type() {

 // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'Projetos', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Projeto', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Projetos', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Projeto', 'twentythirteen' ),
 		'all_items'           => __( 'Todos os Projetos', 'twentythirteen' ),
 		'view_item'           => __( 'View Projetos', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar novo Projeto', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar Projetos', 'twentythirteen' ),
 		'update_item'         => __( 'Update Projetos', 'twentythirteen' ),
 		'search_items'        => __( 'Procurar Projetos', 'twentythirteen' ),
 		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'Projetos', 'twentythirteen' ),
 		'description'         => __( 'Projetos', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'          => array( 'category' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/ 		
 		'menu_icon'   		  => 'dashicons-welcome-write-blog',
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 2,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'page',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'projetos', $args );	


 // Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Membros da Equipe', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Membro da Equipe', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Membros da Equipe', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Membro da Equipe', 'twentythirteen' ),
        'all_items'           => __( 'Todos os Membros da Equipe', 'twentythirteen' ),
        'view_item'           => __( 'View Membro da Equipe', 'twentythirteen' ),
        'add_new_item'        => __( 'Adicionar novo Membro da Equipe', 'twentythirteen' ),
        'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
        'edit_item'           => __( 'Editar Membro da Equipe', 'twentythirteen' ),
        'update_item'         => __( 'Update Membro da Equipe', 'twentythirteen' ),
        'search_items'        => __( 'Procurar Membro da Equipe', 'twentythirteen' ),
        'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
        'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
    );

 // Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Membros da Equipe', 'twentythirteen' ),
        'description'         => __( 'Membros da Equipe', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail' ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
    //  'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */      
        'menu_icon'           => 'dashicons-groups',
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 1,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );

    // Registering your Custom Post Type
    register_post_type( 'equipe', $args );    	

 }

 add_action( 'init', 'custom_post_type', 0 ); 



add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'projetos',
        'title'         => __( 'Imagens', 'cmb2' ),
        'object_types'  => array( 'projetos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );    

	$cmb->add_field( array(
        'name' => 'Lista de imagens',
        'desc' => 'Adicionar as imagens que irão aparecer dentro do projeto',
        'id'   => 'wiki_test_imagens_projeto',
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
            // 'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Adicionar', // default: "Add or Upload Files"
            'remove_image_text' => 'Remover', // default: "Remove Image"
            'file_text' => 'Replacement', // default: "File:"
            'file_download_text' => 'Replacement', // default: "Download"
            'remove_text' => 'Replacement', // default: "Remove"
        ),
    ) );    


}


// =========================== Custom post type	=========================== // 



function granostudio_scripts_child() {

 //Desabilitar jquery
 wp_deregister_script( 'jquery' );  

 // Theme stylesheet.
 wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
 // import fonts (Google Fonts)
 wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
 // Theme front-end stylesheet
 wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

 wp_enqueue_style('front-awasome', get_stylesheet_directory_uri() . '/fonts/font-awesome.min.css');

 wp_enqueue_style('ionicons', get_stylesheet_directory_uri() . '/fonts/ionicons.min.css');

 wp_enqueue_style('carousel-min', get_stylesheet_directory_uri() . '/css/owl.carousel.css'); 

 wp_enqueue_style('carousel-theme', get_stylesheet_directory_uri() . '/css/owl.theme.default.css');  

 wp_enqueue_script('carousel-js', get_stylesheet_directory_uri() . '/js/src/owl.carousel.js', '2.3.4', array('jquery'), true);

 // scripts js
 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);

 // network effet home

}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
