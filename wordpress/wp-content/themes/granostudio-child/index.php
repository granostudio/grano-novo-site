<?php get_header(); ?>

<div class="container-fluid banner-granostudio-bg"> 
    <div class="row padding-banner">
        <div class="col-sm-4 col-md-4 col-lg-3 col-sm-offset-1 col-lg-offset-2">
            <img src="<?php echo get_stylesheet_directory_uri();?>/img/logofinal-negativo.png" class="img-responsive">
        </div>
        <div class="col-sm-6 col-md-4 col-lg-5 col-sm-offset-1 col-lg-offset-1 texto-header">
            <div id="banner">
                <p>Criamos e desenvolvemos</p>                
                <h1 class="typewrite" data-period="2000" data-type='[ "Experiências", "Identidades", "Marcas", "Web Sites", "Materiais Gráficos" ]'>
                    <span class="wrap"></span>
                </h1>
                <p>que geram valor e conversam com seu público<br><br><br></p>
                <a class="btn btn-primary btn-portfolio">Portfólio</a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
    </div>

    <img src="<?php echo get_stylesheet_directory_uri();?>/img/arrow-down.png" class="seta-banner">
</div> 


<div class="projects-clean" data-aos="fade-up" id="portfolio">
    <div class="container">
        
        <div class="intro">
            <h2 class="text-center">Projetos Desenvolvidos</h2>
            <p class="text-center">Valorizamos cada etapa do projeto, da criação a implementação baseando nossas decisões em dados reais e pesquisas de mercado</p>
        </div>
        
        <div class="projects">
 
            <?php
             $args = array( 'post_type' => 'projetos', 'posts_per_page' => 99);
             $loop = new WP_Query( $args );

             if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

            <div class="item col-sm-6 col-md-4">
                <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail" data-aos="fade" data-aos-duration="3000">
                </a>  
                <h3 class="name" data-aos="fade" data-aos-duration="3500"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                <ul class="lista-categoria" data-aos="fade" data-aos-duration="4000">
                    <?php
                      $i = 0;
                      foreach((get_the_category()) as $category) {
                        if(++$i > 3)
                        break;
                        {
                            echo '<li>' . $category->cat_name . '</li>';
                        }
                      }
                     ?>
                </ul><br>
                <a href="<?php echo get_the_permalink(); ?>" class="saiba-mais-post">Saiba mais...</a>
            </div>

            <?php endwhile; // end of the loop. ?>
            <?php endif; ?>

        </div>

    </div>
</div>

<!-- <div class="item col-sm-6 col-md-4">
    <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
    </a>  
    <h3 class="name"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
    <p class="description"><?php echo the_excerpt_max_charlength(250); ?></p>
    <a class="btn btn-primary d-block" role="button" href="<?php echo get_the_permalink(); ?>">Saiba mais</a>
</div> -->
 
<div class="team-boxed" style="background-color:rgb(247,247,247);">
    <div class="container" data-aos="fade">
        <div class="intro">
            <h2 class="text-center">As estrelas do time</h2>
            <p class="text-center">Temos um time de profissionais apaixonados pela mudança e comprometidos com o seu negócio</p>
        </div>

        <div class="row people owl-carousel owl-carousel-projetos">

            <?php
             $args = array( 'post_type' => 'equipe');
             $loop = new WP_Query( $args );

             if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

            <div class="item">
                <div class="box">

                    <?php 

                    $image = get_field('foto_equipe');

                    if( !empty($image) ): ?>

                    <div class="foto-equipe" style="background-image: url('<?php echo $image['url']; ?>');"></div>

                    <?php endif; ?>

                    <h3 class="name"><?php echo get_the_title(); ?></h3>
                    <p class="title"><?php the_field('cargo_equipe'); ?></p>
                    <p class="description"><?php the_field('descricao_equipe'); ?></p>
                    <div class="social">

                        <?php 

                        $linkedin = get_field('linkedin_equipe');
                        $instagram = get_field('instagram_equipe');
                        $twitter = get_field('twitter_equipe'); ?>
                        
                        <!-- if logo linkedin -->
                        <?php if( !empty($linkedin) ): ?>

                            <a href="<?php the_field('linkedin_equipe'); ?>" target="_blanck"><i class="fa fa-linkedin"></i></a>

                        <?php else: ?>

                            <a href="" target="_blanck" style="display: none;"><i class="fa fa-linkedin"></i></a>

                        <?php endif; ?>
                        
                        <!-- if logo linkedin -->
                        <?php if( !empty($instagram) ): ?>

                            <a href="<?php the_field('instagram_equipe'); ?>" target="_blanck"><i class="fa fa-instagram"></i></a>

                        <?php else: ?>

                            <a href="" target="_blanck" style="display: none;"><i class="fa fa-instagram"></i></a>

                        <?php endif; ?>
                        
                        <!-- if logo twitter -->
                        <?php if( !empty($twitter) ): ?>

                            <a href="<?php the_field('twitter_equipe'); ?>" target="_blanck"><i class="fa fa-twitter"></i></a>

                        <?php else: ?>

                            <a href="" target="_blanck" style="display: none;"><i class="fa fa-twitter"></i></a>

                        <?php endif; ?>    

                    </div>
                </div>
            </div>

            <?php endwhile; // end of the loop. ?>
            <?php endif; ?>

        </div>

    </div>
</div>


<div class="article-list">
    <div class="container" data-aos="fade">
        
        <div class="intro">
            <h2 class="text-center">Conheça nosso Blog</h2>
            <p class="text-center">Fique por dentro das novidades do mundo do Marketing, Comunicação e Design</p>
        </div>

        <div class="row articles">

            <?php
             $args = array( 'post_type' => 'post', 'posts_per_page' => 3);
             $loop = new WP_Query( $args );

             if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


            <div class="col-sm-6 col-md-4 item">
                <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
                </a> 
                <h3 class="name"><?php echo get_the_title(); ?></h3>
                <p class="description"><?php echo the_excerpt_max_charlength(200); ?></p>
                <a href="<?php echo get_the_permalink(); ?>" class="action" style="background-color:#68a852;">
                    <i class="fa fa-angle-right" style="color:rgb(246,248,251);font-size:17px;padding:0px;/*line-height:-26px;*/"></i>
                </a>
            </div>

            <?php endwhile; // end of the loop. ?>
            <?php endif; ?>

        </div>

        <!-- Pager -->

    </div>

    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4 col-lg-offset-4">
                <button class="btn btn-primary" type="button" onclick="location.href='/blog/'">Ir para o Blog</button>
            </div>
        </div>
    </div>    
</div>


<div class="newsletter-subscribe">
    <div class="container" data-aos="fade">
        <div class="intro">
            <h2 class="text-center">Assine nossa Newsletter</h2>
        </div>
        <!-- <form class="form-inline" method="post">
            <div class="form-group">
                <input class="form-control" type="email" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <button class="btn btn-primary newsletter-subscribe" type="submit">Inscreva-se agora</button>
            </div>
        </form> -->
        <div id="newsletter-grano-46794a131874cdc12019"></div>
        <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
        <script type="text/javascript">
          new RDStationForms('newsletter-grano-46794a131874cdc12019-html', 'UA-70037167-1').createForm();
        </script>
    </div>
</div>


<div class="map-clean"></div>


<?php get_footer(); ?>
 