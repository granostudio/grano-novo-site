<?php get_header(); ?>


<div class="container margem-t-40">   
    <div class="row">

        <?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'category_name' => 'Destaque');
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        <div class="co-sm-12 post-destaque" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">

            <div class="jumbotron" style="">
                <h1><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h1>
                <p><?php echo the_excerpt_max_charlength(150); ?></p>
                <p><a class="btn btn-primary" role="button" href="<?php echo get_the_permalink(); ?>">Saiba Mais</a></p>
            </div>
 
            <div class="mask-destaque"></div>
 
        </div>

        <?php endwhile; // end of the loop. ?>
        <?php endif; ?>

        <div class="col-sm-12">
            <div class="cabecalho">
                <b style="font-family:Muli, sans-serif;font-size:16px;">CATEGORIAS</b>
                <form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	                <div class="form-group">
	                	<label for="search-field">
	                		<i class="fa fa-search" style="color:#68a852;"></i>
	                	</label>
	                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
	                    <a class="btn btn-light action-button" role="button" href="#" style="font-family:Muli, sans-serif;background-color:#68a852;">Buscar</a></div>
	                </div>
	            </form> 
            </div>

            <div class="categorias col-sm-12">
                <?php wp_list_categories( array(
                    'orderby'    => 'name',
                    'title_li' => false
                ) ); ?> 
            </div>            

        </div>           
  
    </div>
</div>
 
 
<div class="article-list">
    <div class="container" data-aos="fade-up">
        <div class="intro"></div>

        <div class="row articles">

            <?php
             $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
             $args = array( 'post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged, 'page' => $paged);
             $loop = new WP_Query( $args );

             if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


            <div class="col-sm-6 col-md-4 item">
                <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
                </a> 
                <h3 class="name"><?php echo get_the_title(); ?></h3>
                <p class="description"><?php echo the_excerpt_max_charlength(200); ?></p>
                <a href="<?php echo get_the_permalink(); ?>" class="action" style="background-color:#68a852;">
                    <i class="fa fa-angle-right" style="color:rgb(246,248,251);font-size:17px;padding:0px;/*line-height:-26px;*/"></i>
                </a>
            </div>

            <?php endwhile; // end of the loop. ?>
            <?php endif; ?>

        </div>

        <!-- Pager -->
        <div class="">
          <ul class="pager">
            <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Posts Anteriores', $loop->max_num_pages); ?></li>
            <li class="next"><?php previous_posts_link( ' Próximos Posts <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
          </ul>
        </div>

    </div>

</div>


<?php get_footer(); ?>		