$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  }); 
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true, 
      autoplay:true,
      nav: true

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".owl-carousel-projetos").owlCarousel({
    items:3,
    loop:true,  
    margin:0,  
    autoplay:true,
    autoplayTimeout:5000,   
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
        576:{
            items:2,
        },
        768:{
            items:2,            
        },
        992:{
            items:3,
        }
    }
  });
});


// Back to Top ================================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================
 

// /adição de classes no elemento =============================

  $(".attachment-large").each(function(){
        $(this).attr("class","img-responsive img-projetos margem-b-5");
  });

// /adição de classes no elemento =============================



// /Função para a barra de progresso de leitura ===============

window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

// /Função para a barra de progresso de leitura ===============



// Menu Animations ============================================

$(document).ready(function () {
    (function($) {                            
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > 600 || $(".menu-hamburger").css('display') == 'block') {
                $('.navbar').css({"margin-top": "0px"});
            } else {
                $('.navbar').css({"margin-top": "-70px"});
            }
        });
  })(jQuery);
});



$( ".navbar-toggle" ).click(function() {
  $( ".menu-hamburger" ).toggle("slow");
  $( ".bar1").toggleClass("change-bar1");
  $( ".bar2").toggleClass("change-bar2");
  $( ".bar3").toggleClass("change-bar3");
});




// Menu Animations ============================================



// /Animação seta ============================================= 

  $('.seta-banner').on('click', function(){
      $('html, body').animate({
          scrollTop: $('.projects-clean').offset().top -100
      }, 500);
    });

  $('.btn-portfolio').on('click', function(){
      $('html, body').animate({
          scrollTop: $('.projects-clean').offset().top -100
      }, 500);
    });

// /Animação seta ============================================= 



// Grano Scroll ANIMATION =====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ===================================


$(document).ready(function(){
  AOS.init({ disable: 'mobile' });
}); 

}); 


       